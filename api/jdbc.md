
# JdbcConnectionBinding

提供数据库操作，对象通过 WebDriverBinding 中的 `createJdbcConnection` 或 `createMysqlConnection` 方法创建。

## query(sql, params)
* `sql` {string} SQL
* `params` {Object[]} 参数
* {Object[]} 返回查询结果列表

列表查询

```js
// 连接数据库
let db = createMysqlConnection('192.168.3.165', 3306, 'oms', 'root', 'ody,123');
// db = createJdbcConnection('jdbc:mysql://192.168.3.165:3306/oms?useUnicode=true&characterEncoding=utf-8', 'root', 'ody,123');

// 查询
let list = db.query('select order_code, user_name, order_amount from so where order_status = ? limit ?', [9000, 10]);

for (let i in list) {
	println(list[i]); // { orderCode: 'xxx', userName: 'xxx', orderAmount: 1}
}

db.close();
```

## queryOnly(sql, params)
* `sql` {string} SQL
* `params` {Object[]} 参数

获取单个值

```js
// 连接数据库
let db = createMysqlConnection('192.168.3.165', 3306, 'oms', 'root', 'ody,123');
// db = createJdbcConnection('jdbc:mysql://192.168.3.165:3306/oms?useUnicode=true&characterEncoding=utf-8', 'root', 'ody,123');

db.queryOnly('select database()', null); // oms

db.close();
```

## update(sql, params)
* `sql` {string} SQL
* `params` {Object[]} 参数
* {number} 返回影响条数

执行增删改

```js
db.update('update user set xxx = ? where id = ?', ['xxx', 1]);
```

## insert(sql, params)
* `sql` {string} SQL
* `params` {Object[]} 参数
* {number} 自增ID，无则返回空

执行新增，返回自增长id

```js
db.insert('insert into so values (?, ?, ?, ?)', [1, 'xxx', 'hello']);
```

## executeDDL(sql)
* `sql` {string} SQL
* {number} 返回影响条数

执行DDL

```js
db.executeDDL("alter table t_user modify `name` varchar(255) not null comment '名称';");
```

## close()

关闭连接
