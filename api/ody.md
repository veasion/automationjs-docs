# 业务封装模块及函数

基于 JS 自动化测试框架扩展的业务模块及函数，方便直接调用封装函数解决问题。

如登录 `ody.login()` 等。

- 业务基础函数在 include 目录下（包含ody / jira 等对象定义）
- 业务模块在 common 目录下（包含http/分页/dialog/检查/代理等模块）
- 业务数据依赖在 common/dependency 目录下（可以直接依赖商品/会员/订单/商家/店铺等基础数据）

## 业务基础函数

### ody

ody 对象提供常用的后台自动化函数，具体查看  include/ody.js

```js
// 根据环境配置账号登录
ody.login();

// 指定用户名密码登录
ody.login('superadmin', '123456');

// 登录并直接跳到菜单页面，platformId = 1 运营平台，2 商家平台
ody.toPage(['name=center_02', 'name=UpEAfkmgtb', 'name=MerchantCategory'], 1);

// 等待 table 列表加载完成
ody.waitForTableLoaded();

// 选择组织架构 > xxx商家
ody.selectTree(null, 'xxx商家');

// 获取 table 中某列的值，如 订单号
ody.getFirstTableFieldValue('订单号');

// tables > 操作 > 审核按钮点击
ody.getFirstTableField('操作').findText('span', '审核').tryClick();

// table 滚动到最右边
ody.scroll(findOne('css=.el-table__body-wrapper'), null, 0);

// 加载 http 模块
ody.loadCommon('http');

// 依赖商品数据
ody.dependency('product', {dataType: 1});
```

### jira

jira 对象提供 JIRA 操作，具体函数见  include/jira.js

```js
// 登录 jira
jira.login();

// 提bug
jira.createIssue('测试', '程序有bug');
```

## 业务模块

调用模块需要先引用模块，如 `ody.loadCommon('dialog, http, pageInfo');` 建议放代码最前面。

### pageInfo

pageInfo 模块，主要操作分页，具体函数见 common/pageInfo.js

```js
ody.loadCommon('pageInfo');

// 获取当前分页组件对象
pageInfo.getPageObj();
// 获取总页数
pageInfo.total();
// 获取分页大小
pageInfo.pageSize();
// 获取当前页
pageInfo.currentPage();
// 改变分页，第2页
pageInfo.changePage(2);
// 分页检查，检查分页是否正确
pageInfo.check();
```

### dialog

dialog 模块，主要操作弹框，如店铺弹框，商家弹框等，具体函数见 common/dialog.js

```js
ody.loadCommon('dialog');

// 店铺选择
let dialog = odyDialog.get('店铺选择');
// 选择一行数据
odyDialog.selectOne(dialog);
// 点击确认按钮
odyDialog.clickButton(dialog, '确认');
```

### http

http 模块，主要操作HTTP请求，如GET/POST请求，具体函数见 common/http.js

```js
ody.loadCommon('http');

http.get('http://www.baidu.com');

http.post('/api/ouser-web/mobileLogin/login.do', { username: 'superadmin', password: '123456'});

let response = http.request('/oms-web/so/list.do', 'POST');
println('response: ' + response.data);
let ut = http.getCookie('ut');
println('ut: ' + ut);
```

### check

check 模块，主要检查元素，断言，具体函数见 common/check.js

```js
ody.loadCommon('check');

// 检查 table 审核状态 是否为 通过
odyCheck.checkColumnsData('审核状态', '通过', 'pr-xxx 检查审核状态是否通过');

// 检查元素是否存在
odyCheck.checkForms([
    {target: 'name=search_chineseName', name: '商品名称'},
    {target: 'name=search_code', name: '商品编码'},
    {target: "name=handleReset", name: '重置按钮'},
    {target: "name=PriceAuditMerchantMpAudit_btn", name: '查询按钮'}
], 'pr-xxx 元素不存在：');
```

### h5

h5 模块，主要操作前端 h5 页面，具体函数见 common/h5.js

```js
ody.loadCommon('h5');

// 手机密码登录
h5.login('17521511630', '123456');
// 手机号自动通过验证码登录
h5.login('17521511630');
// 获取手机号验证码
let captcha = h5.sendCaptcha('17521511630', 1, 3);
println('验证码：' + captcha);
```

### proxy

proxy 模块，主要用于 js 对象代理，具体函数见 common/proxy.js

```js
ody.loadCommon('proxy');

let p = new ProxyAdapter({}, function (obj, name, args, apply) {
	println(name + '方法被代理了 --before');
	let result = apply();
	println(name + '方法被代理了 --after');
	return result;
});

p.hello = function() {
	println('hello~');
}

p.hello();
// hello方法被代理了 --before
// hello~
// hello方法被代理了 --after
```

## 业务数据依赖

基础通用的造数据函数，如需要一个新的商品、店铺则可以引用该模块。

### brand

品牌依赖，提供新建品牌方法，具体见 common/dependency/brand.js

```js
// 依赖品牌数据
let result = ody.dependency('brand');
// 获取新创建的品牌名称
println(result.branchName);
```
### calculation

计量单位依赖，提供新建计量单位方法，具体见 common/dependency/calculation.js

```js
// 依赖计量单位数据
let result = ody.dependency('calculation');

println(result.calculationUnitCode);
println(result.calculationUnitName);
```

### category

类目依赖，提供新建类目方法，具体见 common/dependency/category.js

```js
// 依赖计量单位数据，创建类目为 “苹果” 的后台类目
let result = ody.dependency('category', { categoryName: '苹果', backCategory: true });

println(result.categoryCode);
println(result.categoryName);
```

### member

会员依赖，提供新建会员方法，具体见 common/dependency/member.js

```js
// 依赖会员数据，指定店铺创建
let result = ody.dependency('member', { storeName: 'xxx店铺' });

println(result.mobile);
println(result.nickname);
```

### merchant

商家依赖，提供新建商家方法，具体见 common/dependency/merchant.js

```js
// 依赖商家数据
let result = ody.dependency('merchant', {applyBrand: true, applyCategory: true});

println(result.merchantCode);
println(result.merchantName);
println(result.categoryName);
```

### product

商品依赖，提供新建商品方法，具体见 common/dependency/product.js

```js
// 依赖商品数据
let result = ody.dependency('product', {dataType: 2, merchantName: 'xxx', type: 1});

println(result.productCode);
println(result.productName);
println(result.categoryName);
```

### store

店铺依赖，提供新建店铺方法，具体见 common/dependency/store.js

```js
// 依赖店铺数据，指定商家和渠道
let result = ody.dependency('store', {merchantName: 'xxx', channelName: '自建BBC'});

println(result.storeCode);
println(result.storeName);
```