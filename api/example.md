# 业务场景



## 下拉框选择

示例：交易方式选中 “普通”

![demo_select](images/demo_select.png)

```js
// 方式一（input能准确定位）

// 点击 "交易方式" input 框，让下拉框弹出来
click("name=search_param_orderSource");
// 然后选中 “普通”
ody.select(null, "普通");
```
```js
// 方式二（input不能准确定位）

// 找到 “交易方式” div
let textElement = findText('div', '交易方式', false);
// 找 "交易方式" 右边的 input
let input = ody.findRightSibling(textElement, 'css=input');
// 点击让下拉框弹出来
input.click();
// 然后选中 “普通”
ody.select(null, "普通");
```



## 弹框中选择组织架构

示例：在新增店铺的弹框中选择组织架构为 “商马商家”

![demo_select](images/demo_dialog_tree.png)

```js
// 方式一

// 引入 dialog 模块
ody.loadCommon('dialog');

// 获取 “新增店铺” 的弹框
let dialogElement = odyDialog.get("新增店铺");
// 找到组织架构的 input
let treeInput = dialogElement.findOne('className=vue-treeselect__input');
// 点击，让组织架构树显示出来
treeInput.tryClick();
// 选择组织架构树为 “商马商家”
ody.selectTree(treeInput, '商马商家');
```
```js
// 方式二

// 如果页面中只有一个树结构时可以直接调用选择组织架构树为 “商马商家”
ody.selectTree(null, '商马商家');
```



## 搜索订单号，并点击进入详情页面

示例：搜索订单号为 201209393323565227，并点击进入详情页面

![demo_select](images/demo_order_code.png)

```js
// 订单号
setValue("name=search_data_searchValue", '201209393323565227');
// 点击搜索
click("name=OmsOrderList_Query_query");
// 等待列表数据加载完成
ody.waitForTableLoaded();
// 获取列表订单号第一行第一列字段元素
let orderCodeElement = ody.getFirstTableField('订单号');
// 找到字段下的 a 标签并点击进入详情
orderCodeElement.findOne('css=a').tryClick();

// 如果需要获取订单号的值
// ody.getFirstTableField('订单号').text();

// 如果需要点击发货按钮，同理可以获取 “操作” 列找到 “发货” 按钮并点击
// ody.getFirstTableField('操作').findText('span', '发货').tryClick();
```



## 点击操作-审核按钮-点击审核拒绝

示例：点击列表第一条数据的审核按钮，并点击审核

![demo_audit](images/demo_audit.png)

```js
// 获取列表第一行操作对象，查找审核字符串并点击
ody.getFirstTableField('操作').findText('*', '审核', false).tryClick();
```

![demo_audit_ok](images/demo_audit_ok.png)

```js
ody.loadCommon('dialog');

// 根据标题获取审核弹框
let dialog = odyDialog.get('审核');
// 查找审核弹框下的审核拒绝span对象
let span = dialog.findText('span', '审核拒绝', false);
// 查找审核拒绝按钮左边的单选按钮并点击
ody.findLeftSibling(span, 'css=input[type=radio]').tryClick();
// 查找保存按钮并点击
ody.findButton(dialog, '保存').tryClick();
```



## 店铺选择弹窗选择一条店铺数据

示例：店铺选择弹窗选择一条店铺数据

![demo_select_store](images/demo_select_store.png)

```js
ody.loadCommon('dialog');

// 根据标题获取店铺选择弹框
let dialog = odyDialog.get('店铺选择');
// 选择第一个数据
odyDialog.selectOne(dialog);
sleep(200);
// 点击弹窗下的确定按钮
odyDialog.clickButton(dialog, '确定');

// 注意
// 单选按钮或多选按钮使用 odyDialog.selectOne(dialog);
// 选择一行使用 odyDialog.selectLine(dialog);
```



## 判断搜索条件是否精确查询

示例：判断列表商家是否全部为 “neo商家”

![demo_select](images/demo_merchants.png)

```js
// 方式一

// 获取列表所有商家
let list = ody.getTableColumns('商家'); // ['neo商家', 'neo商家']
// 断言都为 'neo商家'
for (let i in list) {
    assertResult(list[i] === 'neo商家', '断言商家等于neo商家');
}
```
```js
// 方式二

// 引入 check 模块
ody.loadCommon('check');

// 断言列表商家字段是否为 "neo商家"
odyCheck.checkColumnsData('商家', 'neo商家', '断言商家等于neo商家');
```



## 单选按钮选中 “是”

示例：选中 “支持发票” 为 "是"

![demo_select](images/demo_select_yes.png)

```js
// 找到 “支持发票” label
let labelElement = findText('label', '支持发票：', false);
// 获取右边的所有元素
let list = labelElement.rightSibling();
for (let i in list) {
	// 查找 radio 标签
    let radioLabel = list[i].findOne('css=label span.el-radio__label');
    if (radioLabel && radioLabel.text() === '是') {
        radioLabel.tryClick();
        break;
    }
}
```



## 列表滚动

示例：列表不全，需要滚动到右边

![demo_select](images/demo_scroll.png)

```js
// 找到 table body 元素
let tableBody = findOne('css=.el-table__body-wrapper');
// 滚动到最右边
ody.scroll(tableBody, null, 0);
// 滚动到最下边
ody.scroll(tableBody, 0, null);
// 滚动到最右下边
ody.scroll(tableBody, null, null);
// 滚动还原
ody.scroll(tableBody, 0, 0);
// 页面滚动到最底层
ody.scrollEnd();
// 页面滚动到最顶层
ody.scrollHome();
```



## 鼠标移动到元素上

示例：鼠标移动到 "万万商家" 上，显示菜单框，选中 “删除”

![demo_select](images/demo_mouse_over.png)

```js
// 找到树元素下的 “万万商家”，并把鼠标移动到上面
findOne('css=div.left-tree.el-scrollbar').findText('span', '万万商家').mouseOver('.');
// 等待菜单显示
waitForElementDisplayed('css=ul.el-dropdown-menu');
// 点击 “删除”
findOne('css=body > ul.el-dropdown-menu').findText('li', '删除').tryClick();
```



## 上传图片

示例：上传帖子轮播图

![demo_select](images/demo_upload.png)

```js
// 找到 "帖子轮播图" span 标签
let spanElement = findText('label', '帖子轮播图:', false);
// 根据 span 找到右边的 file input
let fileInputElement = ody.findRightSibling(spanElement, 'css=input[type=file]');
// 上传图片, ody.getIcon() 获取默认图片路径
fileInputElement.sendKeys(ody.getIcon());
```



## 获取 iframe 富文本内容

示例：获取帖子详情

![demo_select](images/demo_iframe.png)

```js
// 找到 "帖子详情" span 标签
let spanElement = findText('label', '帖子详情:', false);
// 根据 span 找到右边的 iframe
let iframeElement = ody.findRightSibling(spanElement, 'tagName=iframe');
// iframe xpath target
let iframeTarget = iframeElement.xpath();
// 进入 iframe
iframe(iframeTarget, function() {
    // 获取文本并打印
    println(text('css=body')); // hello ifrmae !
});
```



## 模拟验证码请求接口+操作数据库获取验证码

示例：模拟h5端发送验证码并获取验证码（模拟接口请求+操作数据库）

```js
// 引入http模块
ody.loadCommon('http');

// 随机一个手机号
let mobile = ody.randMobile();

// 模拟请求发送验证码
let response = http.post('http://mb2c.2d9d6-trunk.oudianyun.com/ouser-web/mobileRegister/sendCaptchas.do', {
    type: 1,
    mobile: mobile,
    captchasType: 3
});

log.info('请求响应: ' + JSON.stringify(response.data));
sleep(1000);

// 连接ouser数据库
let db = ody.getMysqlConnection('ouser');

// 查询验证码 sql
let sql = "select captchas from u_captchas where mobile = (select CONCAT('@%^*', TO_BASE64(AES_ENCRYPT(?, '1fi;qPa7utddahWy')))) order by create_time desc limit 1";

// 查询验证码
let captchas = db.queryOnly(sql, [mobile]);

// 关闭数据库连接
db.close();

println('验证码: ' + captchas);
```
