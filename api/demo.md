# Demo 示例

## 百度搜索 “中国”，进入百度百科结果

```js
// demo 演示 中国百度百科
// @author luozhuowei

// 百度 "中国"
baiduSearch('中国');
// 获取搜索结果
let list = findDisplayed('css=div#content_left > div');
// 变量搜索结果
for (let i in list) {
    let element = list[i].findOne("css=h3 > a");
    // 判断结果是否为百度百科
    if (element && element.text().endsWith("百度百科")) {
        // 点击
		element.click();
        // 等待页面加载
        waitForPageLoaded(10);
        // 切换到新打开的窗口
        switchToNextWindow();
        break;
	}
}

function baiduSearch(str) {
    open("https://www.baidu.com");
    sendKeys('id=kw', str);
    click("css=input[value='百度一下']");
    waitForPageLoaded(5);
}
```



## 起点小说网搜索并下载斗破苍穹

```js
// demo 演示 爬小说
// @author luozhuowei

let bookName = '斗破苍穹';
let savePath = 'C:\\Users\\user\\Desktop\\斗破苍穹.txt';

// 起点小说
open('https://www.qidian.com');
// 搜索
sendKeys('id=s-box', bookName);
tryClick('id=search-btn');
switchToNextWindow();
waitForPageLoaded();

// 点击搜索结果
findOne('css=#result-list').findOne('linkText=' + bookName).tryClick();
switchToNextWindow();
waitForPageLoaded();

// 点击“免费试读”
findText('a', '免费试读', false).tryClick();
waitForPageLoaded();

// 关闭广告弹窗
tryClick('css=a.lbf-panel-close');
sleep(200);

do {
    // 小说内容
    let bookBody = findOne('css=#j_readMainWrap div.text-wrap div.main-text-wrap div.read-content.j_readContent');
    // 移除评论
    removeElements(bookBody.findDisplayed('css=span.review-count'));
    // 小说内容
    let context = bookBody.text().replace(/[\n]/g, '\r\n\r\n') + '\r\n\r\n';
    // 保存小说
    writeText(savePath, context, true);
    // 下一章
    let nextPage = findOne('id=j_chapterNext');
    if (nextPage) {
        nextPage.tryClick();
        waitForPageLoaded();
    } else {
        break;
    }
} while (true);

function removeElements(elements) {
    if (!elements) return;
    for (let i in elements) {
        if (!elements[i]) continue;
        executeScriptByParams('arguments[0].remove();', [elements[i]]);
    }
}
```

## 新增一个会员
```js
// demo 演示 新增一个会员
// @author luozhuowei
ody.loadCommon('dialog');

// 登录: 会员中心 -> 会员管理 -> 会员列表
ody.toPage(["css=span[name='center_09']", "css=span[name='3089']", "css=span[name='OuserCenterMemberList']"], 1);

// 随机手机号
let randomMobile = ody.randMobile();

// 新增一个会员
addMember(randomMobile);

// 查询会员
queryByMobile(randomMobile);

// 新增会员
function addMember(mobile) {
    click("name=OuserCenterMemberListAdd_saveMemberInit");
    sleep(500);
    assertResult(findOne("css=i.close-dialog.el-icon-close") != null, "pr-22009:05新增会员按钮：点击新增按钮后会有弹框");
    type("name=saveMemberInfo_mobile", mobile);
    type("name=saveMemberInfo_nickname", randCode(8));
    click("css=input[name='saveMemberInfo_storeName'].el-input__inner");
    let store = odyDialog.get('店铺选择');
    assertResult(store != null, '会员列表点击添加》店铺选择弹窗未找到');
    sleep(200);
    odyDialog.selectLine(store);
    sleep(200);
    odyDialog.clickButton(store, '确定');
    click("name=OuserCenterMemberSaveMember_saveMember");
    sleep(1000);
    ody.waitForTableLoaded();
}

// 查询会员是否存在
function queryByMobile(mobile) {
    findOne("name=searchForm_mobile5").setValue(mobile);
    tryClick("name=OuserCenterMemberListQuery_handleQuery");
    sleep(200);
    waitForPageLoaded();
}
```

## 分销商品管理

```js
// @author luozhuowei
// @date 2020-09-25
ody.loadCommon('check, dialog, pageInfo');

if (toPage()) {
    // 添加分销商品
    addAgentProduct();
    // 检查元素
    checkSearchForm();
    // 操作
    operate();
    // 删除分销商品
    deleteProduct();
}

function toPage() {
    // 分销中心 > 分销商品管理
    return ody.toPage(['name=center_12', 'name=DistributionProduct'], 1, false);
}

function addAgentProduct() {
    pageInfo.setPageObj(null);
    let total = pageInfo.total();
    click("name=DistributionProductAdd_addDistributionProduct");
    ody.waitForTableLoaded();
    let dialog = odyDialog.get('添加商品');
    assertResult(dialog != null, 'pr-35379 : 未找到添加商品弹框');
    pageInfo.setPageObj(dialog.findOne("css=div.el-pagination"));
    if (pageInfo.total() === 0) {
        // 依赖商品
        ody.dependency('product', {dataType: 2});
        toPage();
        addAgentProduct();
        return;
    }
    odyDialog.selectOne(dialog);
    click("name=DistributionProductAdd_handleOk");
    waitForElementNotDisplayed("className=el-dialog__body");
    ody.waitForTableLoaded();
    pageInfo.setPageObj(null);
    assertResult(pageInfo.total() > total, 'pr-35379 : 添加商品失败');
}

function checkSearchForm() {
    odyCheck.checkForms([
        {target: 'name=queryParam_mpName', name: '商品名称'},
        {target: 'name=queryParam_mpCode', name: '商品编码'},
        {target: "css=input[name='queryParam_merchantId'].el-input__inner", name: '商家名称'},
        {target: "css=input[name='queryParam_storeId'].el-input__inner", name: '店铺名称'}
    ], 'pr-35370 : 查询条件-');
    assertResult(pageInfo.total() > 0, '分销商品管理无数据！！！');
    let data = ody.getFirstTableFieldValue('商品编码');
    type("name=queryParam_mpCode", data);
    click("name=DistributionProductListQuery_handleQuery");
    odyCheck.checkColumnsData('商品编码', data, 'pr-35371 : 查询条件-商品编码：精确查询');
    reset();
    data = ody.getFirstTableFieldValue('商品名称');
    type("name=queryParam_mpName", data);
    click("name=DistributionProductListQuery_handleQuery");
    odyCheck.checkColumnsData('商品名称', function (text) {
        return text.indexOf(data) !== -1;
    }, 'pr-35370 : 查询条件-商品名称：全模糊查询');
    reset();
    assertResult(true, 'pr-35374 : 重置按钮：点击重置按钮，输入框内容清空，选择框回到默认状态');
}

function operate() {
    let set_dialog = null;
    try {
        ody.getFirstTableField('操作').findText('span', '设置').tryClick();
        sleep(100);
        set_dialog = odyDialog.get('设置商品信息');
        assertResult(set_dialog != null, 'pr-35403 : 设置-点击按钮，显示设置商品信息页');
    }catch (e) {
        assertResult(false, 'pr-35403 : 设置-点击按钮，显示设置商品信息页 》 设置按钮不存在');
    }
    try {
        findOne('name=dpsForm_expiryDateType').findOne("css=input[value='2']").tryClick();
        let date = new Date();
        findOne("name=dpsForm_expiryStartTimeStr").setValue(formatDate(date, 'yyyy-MM-dd HH:mm:ss'));
        date.setFullYear(date.getFullYear() + 1);
        findOne("name=dpsForm_expiryEndTimeStr").setValue(formatDate(date, 'yyyy-MM-dd HH:mm:ss'));
        click("css=input[name='dpsForm_priority'].el-input__inner");
        assertResult(true, 'pr-35405 : 设置-有效期：必填，可选 永久有效 、指定时间');
    } catch (e) {
        assertResult(false, 'pr-35405 : 设置-有效期：必填，可选 永久有效 、指定时间');
    }
    click("name=DistributionProductSet_handleSave");
    ody.waitForTableLoaded();
}

function deleteProduct() {
    try {
        pageInfo.setPageObj(null);
        let total = pageInfo.total();
        if (total === 0) {
            addAgentProduct();
            deleteProduct();
            return;
        }
        ody.getFirstTableField('操作').findText('span', '删除').tryClick();
        odyDialog.clickMessageBox('确定');
        ody.waitForTableLoaded();
        assertResult(pageInfo.total() < total, 'pr-35401 : 删除-删除成功后，删除的记录不再显示在分销商品列表');
    } catch (e) {
        assertResult(false, 'pr-35401 : 删除-删除成功后，删除的记录不再显示在分销商品列表');
    }
}

function reset() {
    click("name=reset");
    click("name=DistributionProductListQuery_handleQuery");
    ody.waitForTableLoaded();
}
```